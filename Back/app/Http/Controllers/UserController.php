<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function create(Request $request){
        $users = new User;
        $users->create($request);
        return response()->json(['user'=> $users]);
    }

    public function index(){
        $users = User::all();
        return response()->json(['user'=> $users]);      
    }

    public function show($id){
        $users = User::find($id);
        return response()->json(['user'=> $users]);
    }

    public function update(Request $request, $id){
        $users = User::find($id);
        $users->updateUser($request);
        return response()->json(['user'=> $users]);
    }

    public function delete($id){
        $users = User::find($id);
        $users->delete();
        return response()->json(['Usuário deletado com sucesso!'=> $users]);    
    }
}
