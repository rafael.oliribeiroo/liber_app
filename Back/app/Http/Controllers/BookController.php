<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    public function create(Request $request){
        $books = new Book;
        $books->create($request);
        return response()-> json(['book' => $books], 200);
    }

    public function index(){
        $books = Book::all();
        return response()-> json(['book' => $books], 200);      
    }

    public function show($id){
        $books = Book::find($id);
        return response()-> json(['book' => $books], 200);
    }

    public function update(Request $request, $id){
        $books = Book::find($id);
        $books->updateBook($request);       
        return response()-> json(['book' => $books], 200);
    }

    public function delete($id){
        $books = Book::find($id);
        $books->delete();    
        return response()-> json(['Livro deletado com sucesso!' => $books], 200);
    }
}
