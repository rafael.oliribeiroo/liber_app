<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    use HasFactory;

    public function create(Request $request){
        $this->name = $request->name;
        $this->description = $request->description;
        $this->gender = $request->gender;
        $this->author_name = $request->author_name;
        $this->user_id = $request->user_id;
        $this->save();
    }

    public function updateBook(Request $request){
        if($request->name){
            $this->name = $request->name;
        }
        if($request->description){
            $this->description = $request->description;
        }
        if($request->gender){
            $this->gender = $request->gender;
        }
        if($request->author_name){
            $this->author_name= $request->author_name;
        }        
        $this->save();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
