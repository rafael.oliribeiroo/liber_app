<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\http\Controllers\BookController;
use App\http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('books', [BookController::class,'create']);
Route::get('books', [BookController::class,'index']);
Route::get('books/{id}', [BookController::class,'show']);
Route::put('books/{id}', [BookController::class,'update']);
Route::delete('books/{id}', [BookController::class,'delete']);

Route::post('users', [UserController::class,'create']);
Route::get('users', [UserController::class,'index']);
Route::get('users/{id}', [UserController::class,'show']);
Route::put('users/{id}', [UserController::class,'update']);
Route::delete('users/{id}', [UserController::class,'delete']);
    